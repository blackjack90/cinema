<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="d"  %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="now" class="java.util.Date" scope="page"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Film</title>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/menu.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/film.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/main.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/rating.css"/>
</head>
<body onload="setUserRateOnPageLoad()">
<div class="wrapper">
    <header class="top-column">
        <div>
            <a href="/" class="logo"><img src="" alt=""/>LOGO</a>
        </div>
        <nav class="head-menu">
            <ul>
                <li><a href="page1.html">Main menu</a></li>
            </ul>
        </nav>
        <div class="authorization">
            <c:if test="${auth.email != '0'}">
                <c:out value="${auth.email}" />
            </c:if>
            <d:choose>
                <d:when test="${auth.email != null}">
                    <a id="auth-user" href="#">${auth.email}</a>
                    <a id="exit-user" href="#">Exit</a>
                </d:when>
                <d:otherwise>
                    <a id="sign-in" href="#">Sign in</a>
                    <a id="authorization" href="#">Authorization</a>
                </d:otherwise>
            </d:choose>
        </div>
    </header>
    <aside class="left-col">
        <nav>
            <ul>
                <li><a href="page1.html">Страница 1</a></li>
                <li><a href="page2.html">Страница 2</a></li>
            </ul>
        </nav>
    </aside>
    <main class="right-col">
        <section>
            <h1>${film.name}</h1>
            <div style="float: left; margin: 10px;">
                <p><img width="280" height="400" src="/film/get-film-image/${film.filmId}"></p>
                <p>
                    <form action="${pageContext.request.contextPath}/film/process-adding-image/${film.filmId}" enctype="multipart/form-data" method="post">
                <p><input type="file" name="file">
                    <input type="submit" value="upload">
                </p>
                </form>
                </p>
                <p>${message}</p>
            </div>

            <%--Rating--%>
            <div class="stars">
                <p>Рейтинг фильма:<div id="film-rating"></div></p>
                <p>Ваша оценка:</p>
                <span>
                    <label class="star star-5" for="star-5">
                        <img data-id="5" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                    </label>
                    <input class="star star-5" id="star-5" type="radio" name="star"/>
                </span>
                <span>
                    <label class="star star-4" for="star-4">
                        <img data-id="4" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                    </label>
                    <input class="star star-4" id="star-4" type="radio" name="star"/>
                </span>
                <span>
                    <label class="star star-3" for="star-3">
                        <img data-id="3" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                    </label>
                    <input class="star star-3" id="star-3" type="radio" name="star"/>
                </span>
                <span>
                    <label class="star star-2" for="star-2">
                        <img data-id="2" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                    </label>
                    <input class="star star-2" id="star-2" type="radio" name="star"/>
                </span>
                <span>
                    <label class="star star-1" for="star-1">
                        <img data-id="1" onclick="changeState(this)" class="raiting" src="${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg">
                    </label>
                    <input class="star star-1" id="star-1" type="radio" name="star"/>
                </span>
            </div>

            <div style="float: left; margin: 10px;">
                <d:forEach var="uDate" items="${uniqueDates}">
                    <fmt:formatDate pattern='dd-MM' type='date' value='${uDate}' var="uniqueDate"/>
                    <p>${uniqueDate}</p>
                    <d:forEach var="session" items="${film.sessionList}">
                        <fmt:formatDate pattern='dd-MM' type='date' value='${session.sessionStartDate}' var="sessionDate"/>
                        <fmt:formatDate pattern='HH:mm' type='time' value='${session.sessionStartTime}' var="sessionTime"/>
                        <d:choose>
                            <d:when test="${uniqueDate == sessionDate}">
                                <div style="float: left; margin: 10px;">
                                    <p><a href="${pageContext.servletContext.contextPath}/movie/session/${session.movieSessionId}">${sessionTime}</a></p>
                                </div>
                            </d:when>
                            <d:otherwise/>
                        </d:choose>
                    </d:forEach>
                </d:forEach>
            </div>
        </section>
        <section>
            <d:choose>
                <d:when test="${auth.id != '0'}">
                    <d:out value="${auth}"></d:out>
                </d:when>
                <d:otherwise></d:otherwise>
            </d:choose>
        </section>
    </main>
    <div id="form-container" class="form-container" hidden>
        <form id="login-form" class="auth-form" action="${pageContext.servletContext.contextPath}/user" method="post" hidden>
            <input id="userEmail" name="userEmail" type="email" placeholder="email" required>
            <input id="userPass" name="userPass" type="password" placeholder="password" required>
            <input type="submit" value="ok">
        </form>
        <form id="auth-form" class="auth-form" action="" hidden>
            <input type="email" placeholder="email" required>
            <input type="password" placeholder="password" required>
            <input type="tel" placeholder="phone" required>
            <input type="text" placeholder="name" >
        </form>
        <button id="remove-form" id="remove-form" onClick="removeForm()">Cancel</button>
    </div>

    <footer>
        <p>Копирайты</p>
    </footer>
</div>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/main.js"></script>

<script>
    document.onload = setUserRateOnPageLoad();

    function setUserRateOnPageLoad() {
    //set average with only 2 nums after dot
    let avgRating = "${averageRating}";
    let parsedavgRating;

    if(avgRating ^ 0 !== avgRating) { //check that num is not an Integer
        parsedavgRating = Math.floor(avgRating * 10) / 10;
    }else{
        parsedavgRating = avgRating;
    }

    document.getElementById("film-rating").innerHTML = parsedavgRating;

    //set rating that user setted before
    let usrRate = "${userRate.value}"
    let stars = document.querySelectorAll('img.raiting');

    console.log(stars.length);

    for(let i = stars.length-1; i >= 0; i--) {
        let star = stars[i];

        if(usrRate > stars.length-1 - i){
            star.src = "${pageContext.servletContext.contextPath}/resources/images/star-active.jpg";
        }
    }
    }

    var lastRaiting = null;
    function changeState(star) {
        var value = star.dataset.id;
        console.log(value);
        setRaiting(value);

        var userId = ${userId};
        var filmId = "${film.filmId}";

        //send user's rate to server
        var sendRateUrl = 'http://localhost:8080/rate/getRating';
        var rateId = ${userRate.rateId}
        fetch(sendRateUrl, {
            method : "post",
            credentials: 'include',

            headers: {
                "Accept":"application/json",
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({rateId, value, filmId, userId})
        })
            .then(res => res.json())
            .then(res => {
                //update average rating after user vote
                console.log(res)

                let avgRating = res;
                let parsedavgRating;

                if(avgRating ^ 0 !== avgRating) { //check that num is not an Integer
                    parsedavgRating = Math.floor(avgRating * 10) / 10;
                }else{
                    parsedavgRating = avgRating;
                }

                document.getElementById("film-rating").innerHTML = parsedavgRating;
            })
    }

    function setRaiting(raiting) {
        let stars = document.querySelectorAll('img.raiting');

        for(let i = stars.length-1; i >=0; i--) {
            let star = stars[i];

            if(star.dataset.id <= raiting && raiting != lastRaiting) {
                star.src = "${pageContext.servletContext.contextPath}/resources/images/star-active.jpg";
                star.setAttribute("checked", true);
            } else {
                star.src = "${pageContext.servletContext.contextPath}/resources/images/star-inactive.jpg";
                star.removeAttribute("checked");
            }
        }

        if(raiting == lastRaiting) {
            lastRaiting = null;
        } else lastRaiting = raiting;

    }
</script>
</body>
</html>
