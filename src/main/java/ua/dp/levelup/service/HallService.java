package ua.dp.levelup.service;

import ua.dp.levelup.core.model.Hall;

import java.util.List;

public interface HallService {

    void createHall(Hall hall);
    void updateHall(Hall hall);
    void deleteHall(Hall hall);
    Hall getHallById(Long id);
    Hall getHallByNumber(int hallNumber);

    List<Hall> getAllHalls();
}
