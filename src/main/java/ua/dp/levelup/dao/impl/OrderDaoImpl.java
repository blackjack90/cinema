package ua.dp.levelup.dao.impl;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.Ticket;
import ua.dp.levelup.dao.OrderDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class OrderDaoImpl implements OrderDao {

    @Autowired
    private HibernateTemplate template;

    @Override
    public void createOrder(Order order, List<Ticket> tickets) {
        Session session = template.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        order.setTickets(tickets);
        template.save(order);

        for (Ticket ticket : tickets) {
            ticket.setOrder(order);
            template.save(ticket);
        }

        transaction.commit();
    }

    @Override
    public void updateOrder(Order order) {
        template.update(order);
    }

    @Override
    public Order getOrderById(long orderId) {
        Session session = template.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();
        Order order = template.get(Order.class, orderId);
        List<Ticket> tickets = order.getTickets();

        System.out.println(tickets.size());
        transaction.commit();
        return order;
    }

    @Override
    public List<Order> getAllOrders() {
        return template.loadAll(Order.class);

    }

    @Override
    public List<Order> getAllOrdersForToday() {

        String query = "from Order o where o.orderDate = :currentDate";
        return (List<Order>)template.findByNamedParam(query, "currentDate", new Date());
    }


}
