package ua.dp.levelup.dao.impl;

import org.hibernate.Hibernate;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Property;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.HibernateTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.dp.levelup.core.model.Hall;
import ua.dp.levelup.core.model.MovieSession;
import ua.dp.levelup.dao.HallDao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Date;
import java.util.List;

@Transactional
@Repository("hallDao")
public class HallDaoImpl implements HallDao {

    private EntityManager entityManager;

    @Autowired
    private HibernateTemplate hibernateTemplate;

    @PersistenceContext(unitName="emf")
    public void setEntityManager(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void createHall(Hall hall) {
        hibernateTemplate.save(hall);
    }

    @Override
    public void updateHall(Hall hall) {
        hibernateTemplate.update(hall);
    }

    @Override
    public void deleteHall(Hall hall) {
        hibernateTemplate.delete(hall);
    }

    @Override
    public List<Hall> getAllHalls() {
        List<Hall> halls = hibernateTemplate.loadAll(Hall.class);
        for(Hall hall : halls){
            Hibernate.initialize(hall.getRows());
        }
        return halls;
    }
    @Override
    public Hall getHallById(Long id) {
        return hibernateTemplate.get(Hall.class, id);
    }

    @Override
    public Hall getHallByNumber(int hallNumber) {
        CriteriaBuilder builder = entityManager.getEntityManagerFactory().getCriteriaBuilder();
        CriteriaQuery<Hall> criteriaQuery = builder.createQuery(Hall.class);
        Root<Hall> root = criteriaQuery.from(Hall.class);

        criteriaQuery.select(root);
        criteriaQuery.where(builder.equal(root.get("hallNumber"), hallNumber));

        return entityManager.createQuery(criteriaQuery).getSingleResult();
    }


}