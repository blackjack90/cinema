package ua.dp.levelup.core.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@ToString(exclude = "sessionList")
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "FILMS")
public class Film {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long filmId;
  private String name;
  private String description;
  private double duration;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "film", cascade = CascadeType.ALL)
  @JsonIgnore
  private List<MovieSession> sessionList;

  private String imageName;

  public Film(String name, String description, double duration) {
    this.name = name;
    this.description = description;
    this.duration = duration;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Film film = (Film) o;

    return filmId.equals(film.filmId);

  }

  @Override
  public int hashCode() {
    return filmId.hashCode();
  }
}
