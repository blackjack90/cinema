package ua.dp.levelup.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.*;
import ua.dp.levelup.service.MovieSessionService;

import javax.persistence.ManyToOne;
import java.util.*;

/**
 * Created by unike on 31.07.2017.
 */

@Controller
@RequestMapping("/basket")
public class BasketController {

    @Autowired
    private MovieSessionService movieSessionService;

    Map<Long, Map<Long, List<Ticket>>> allTicketsOfClient = new HashMap<>();
    Long clientId = 1L;

    @RequestMapping(value = "/test", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity test(@RequestBody List<Ticket> tickets) {
        for (Ticket t:tickets){
            System.out.println(t.toString());
        }
        if (allTicketsOfClient.get(clientId) == null){
            allTicketsOfClient.put(clientId, new HashMap<>());
        } else {
            allTicketsOfClient.get(clientId).remove(tickets.get(0).getMovieSessionId());
            allTicketsOfClient.get(clientId).put(tickets.get(0).getMovieSessionId(), tickets);
        }
        return new ResponseEntity(HttpStatus.OK);
    }


    @RequestMapping(value = "/")
    public ModelAndView basket() {
        ModelAndView modelAndView = new ModelAndView("view-basket");
        Map<Long, List<Ticket>> allTicketsBymvisessionsIDsOfClient = allTicketsOfClient.get(clientId);
        Set<Long> seansesIDs = allTicketsBymvisessionsIDsOfClient.keySet();
        List<MovieSession> allMovieSessions = movieSessionService.getAllMovieSessions();
        modelAndView.addObject("clientTickets",allTicketsBymvisessionsIDsOfClient);
        modelAndView.addObject("seansesIDs", seansesIDs);
        modelAndView.addObject("allMovieSessions", allMovieSessions);
        return modelAndView;
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST, consumes = "application/json")
    public ResponseEntity buy(@RequestBody List<Ticket> tickets) {
        for (Ticket t:tickets){
            System.out.println("test of buy " +t.toString());
        }

        allTicketsOfClient.put(clientId,new HashMap<>() );
        return new ResponseEntity(HttpStatus.OK);
    }


}
