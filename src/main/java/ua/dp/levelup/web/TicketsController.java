package ua.dp.levelup.web;

import com.google.zxing.WriterException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ua.dp.levelup.core.model.Ticket;
import ua.dp.levelup.service.OrderService;
import ua.dp.levelup.service.TicketService;
import ua.dp.levelup.utils.GenerationQRCode;

import java.util.List;

/**
 * Created by andreypo
 */
@Controller
@RequestMapping("/ticket")
public class TicketsController {

    @Autowired
    private OrderService orderService;

    @Autowired
    private TicketService ticketService;

    @Autowired
    private GenerationQRCode generationQRCode;

    @RequestMapping(value = "/buy", method = RequestMethod.GET)
    public ModelAndView getBuyTicketsPage() {
        List<Ticket> tickets = ticketService.getAllTickets();
        ModelAndView modelAndView = new ModelAndView("buy-ticket");

        modelAndView.addObject("allTickets", tickets);
        return modelAndView;
    }

    @RequestMapping(value = "/buy", method = RequestMethod.POST, consumes = "application/json")
    public String buyTickets(@RequestBody Ticket... tickets) {
        orderService.createOrder(tickets);
        return "redirect:list";
    }

    @RequestMapping(value = "/qr", method = RequestMethod.GET)
    public ResponseEntity generateQRCode() throws WriterException {
        generationQRCode.generationQRCode("https://trello.com/b/6jj6D0xL/cinema", 1);
        return new ResponseEntity(HttpStatus.OK);
    }
}
