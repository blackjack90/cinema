package ua.dp.levelup.web.controllers;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.dp.levelup.core.model.Order;
import ua.dp.levelup.core.model.Ticket;
import ua.dp.levelup.service.EmailManagementService;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Arrays;

@Controller
@RequestMapping("/notify")
public class NotificationController {

    private static final Logger LOGGER = Logger.getLogger(NotificationController.class);

    @Autowired
    private EmailManagementService emailManagementService;


    @RequestMapping("/ticketSold")
    public ResponseEntity<HttpStatus> ticketSold() {
        Ticket ticket = new Ticket(323L, 25D, 21L, 11, 8, 5L);
        Order order = new Order(54256L, Arrays.asList(ticket), 12L);

        try {
            emailManagementService.sendNotificationEmailAfterTicketBuy("alexandr.shegeda@gmail.com", order);
        } catch (MessagingException | IOException e) {
            LOGGER.error(e);
        }

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
