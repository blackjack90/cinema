package ua.dp.levelup.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ua.dp.levelup.core.model.User;
import ua.dp.levelup.core.model.message.EmailText;
import ua.dp.levelup.service.EmailManagementService;
import ua.dp.levelup.service.UserService;
import ua.dp.levelup.utils.CinemaUtilityFunctions;


@Controller
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = Logger.getLogger(UserController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private EmailManagementService emailManagementService;
    @Autowired
    private CinemaUtilityFunctions cinemaUtilityFunctions;

    @Value("${http.url}")
    private String httpUrl;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public ModelAndView getRegistrationPage() {
        ModelAndView modelAndView = new ModelAndView("registration");
        modelAndView.addObject("session", new User());
        return modelAndView;
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST, consumes = "application/json")
    public ModelAndView addUserFromRegistration(@RequestBody User user) {
        String alertMessage;
        User user1 = userService.getUserByEmail(user.getEmail());
        if (null == user1) {
            String confirmCode = cinemaUtilityFunctions.generateConfirmCode();
            user.setConfirmCode(confirmCode);
            userService.createUser(user);
            String emailMessage = "Good day! You are registered in Cinema. To verify the email" +
                    " please click on " + httpUrl + "/user/registration/confirmation/" + confirmCode;
            emailManagementService.addMessageToQueue(new EmailText(user.getEmail(), "Registration on Cinema", emailMessage));
            alertMessage = "To complete your registration, please, follow the link you will receive within 5 min on specified email";
        } else {
            alertMessage = "This email is already registered in the Cinema";
        }
        LOGGER.info("Registration " + user.getEmail() + " " + alertMessage);
        return new ModelAndView("registration-popup", "message", alertMessage);
    }

    @RequestMapping(value = "/registration/confirmation/{code}", method = RequestMethod.GET)
    public ModelAndView confirmEmail(@PathVariable(name = "code") String code, RedirectAttributes redirectAttributes) {
        String message;
        User user = userService.getUserByConfirmCode(code);
        if (null != user) {
            user.setConfirmCode("");
            userService.updateUser(user);
            message = "Congratulation! Your registration completed successfully!";
            LOGGER.info("Confirmation " + user.getEmail() + " " + message);
        } else {
            message = "Error! There is no such confirmation code!";
            LOGGER.info(message);
        }
        return new ModelAndView("registration-popup", "message", message);
    }
}
